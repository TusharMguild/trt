jQuery(document).ready(function($) {

// Mobile search
/*
(function($){
  
  $(function(){
    
      $('html').toggleClass('no-js js');
    
    $('.search-toggle-box .search-toggle').click(function(e){
        e.preventDefault();
        
        $(this).next('.search-content').slideToggle();
    });
    
  });
  
})(jQuery);*/

// MOBILE TOGGLE
	$('#mobile-toggle , #mobile-overlay').click(function() { $( "body" ).toggleClass('mobile-expand');console.log("Mobile Toggled");$(this).toggleClass('icon-chevron-right').toggleClass('icon-bars'); });

$('#mobile-nav .sub-menu').slideUp(0);
$('#mobile-nav .menu-item-has-children').prepend('<i class="icon-chevron-down"></i>');
$('#mobile-nav .menu-item-has-children').click(function() {
	$(this).toggleClass('sub-menu-expanded').children('.sub-menu').slideToggle(400);
});
$('#mobile-nav i').click(function() {
	$(this).children('.sub-menu').slideToggle(400);
});
// ADD TO SOCIALBOX

$('.social_link li').each(function() {
	if ($(this).hasClass('socialbox_phone')) {
		$(this).find('a').prepend('<i class="icon-envelope"></i>');
	}
	if ($(this).hasClass('socialbox_addr')) {
		$(this).find('a').prepend('<i class="icon-map-marker"></i>');
	}
	if ($(this).hasClass('socialbox_fax')) {
		$(this).find('a').prepend('<i class="icon-fax"></i>');
	}
	if ($(this).hasClass('socialbox_email')) {
		$(this).find('a').prepend('<i class="icon-envelope"></i>');
	}
});

$('.woocommerce .thumbnails a, .woocommerce .woocommerce-main-image').removeAttr('href');

var oldImageSrc = $('.woocommerce .woocommerce-main-image').find('img').attr('src');
var oldImage = '<a><img src="'+oldImageSrc+'" url="'+oldImageSrc+'"></a>';
$('.woocommerce .thumbnails').prepend(oldImage);
$('.woocommerce .thumbnails a').click(function() {
	var newImageSrc = $(this).find('img').attr('url');
	var newImage = '<a><img src="'+newImageSrc+'" url="'+newImageSrc+'"></a>';

	//var oldImageSrc = $('.woocommerce .woocommerce-main-image').find('img').attr('src');
	//var oldImage = '<img src="'+oldImageSrc+'" url="'+oldImageSrc+'">';
	
	$('.woocommerce .woocommerce-main-image').html(newImage);
	//$(this).html(oldImage);
});

$('#inquire-button, #call-us-pricing').click(function() {
	$('#modalform').fadeIn();
});

$('#modalform i, #modaloverlay').click(function() {
	$('#modalform').fadeOut();
});

});


