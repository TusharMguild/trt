<?php 
/*
* Template Name: Featured Product
*/
get_header(); ?>
<section id="content">
<div class="page-wrapper">
<main id="main-content" class="clearfix" role="main">
			<h1 class="page-title"><?php the_title(); ?></h1>

		<?php 
		global $wp_query;
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args = array( 
			'post_type' => 'product',
			'post_status'      => 'publish',
			'posts_per_page' => 12,
			'meta_query' => array(
                  array(
                      'key'       => 'product_visible',
                      'value'     => '0',
                      'compare'   => '==',
                      ),
                    ),
            'tax_query' => array(
                    array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                    ),
                ),            
            'paged'    => $paged, 
		);
		query_posts( $args ); ?>
			<p class="woocommerce-result-count">
	<?php
	/*$paged    = max( 1, $wp_query->get( 'paged' ) );
	$per_page = $wp_query->get( 'posts_per_page' );
	$total    = $wp_query->found_posts;
	$first    = ( $per_page * $paged ) - $per_page + 1;
	$last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );*/

	if ( $total <= $per_page || -1 === $per_page ) {
		/* translators: %d: total results */
		//printf( _n( 'Showing the single result', 'Showing all %d results', $total, 'woocommerce' ), $total );
	} else {
		/* translators: 1: first result 2: last result 3: total results */
		//printf( _nx( 'Showing the single result', 'Showing %1$d&ndash;%2$d of %3$d results', $total, 'with first and last result', 'woocommerce' ), $first, $last, $total );
	}
	?>
</p>

				<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked wc_print_notices - 10
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/**
						 * woocommerce_shop_loop hook.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
						do_action( 'woocommerce_shop_loop' );
					?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
			/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
			$total   = $wp_query->max_num_pages;
			$current = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) );
			$format  = isset( $format ) ? $format : '';

			if ( $total <= 1 ) {
				return;
			}
			?>
			<nav class="woocommerce-pagination">
				<?php
					echo paginate_links( apply_filters( 'woocommerce_pagination_args', array( // WPCS: XSS ok.
						'base'         => $base,
						'format'       => $format,
						'add_args'     => false,
						'current'      => max( 1, $current ),
						'total'        => $total,
						'prev_text'    => '&larr;',
						'next_text'    => '&rarr;',
						'type'         => 'list',
						'end_size'     => 3,
						'mid_size'     => 3,
					) ) );
				?>
			</nav>
		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php
				/**
				 * woocommerce_no_products_found hook.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
			?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
	
	</main>
	</div>
</section>	
<?php get_footer(); ?>