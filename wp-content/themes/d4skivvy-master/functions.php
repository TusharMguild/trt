<?php #13May15
	include 'lib/skivvy_func_auto.php';		// Auto Functions
	include 'lib/skivvy_func_user.php';		// Usable Functions
	include 'lib/admin/index.php';			// Admin Functions & styling
	include 'lib/skivvy_shortcodes.php';	// Shortcodes
	include 'inc/skivvy_simple.php';		// Cleanup
	include 'inc/skivvy_register.php';		// Registry
	include 'inc/woocommerce.php';

class Menu_With_Description extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<br /><span class="sub">' . $item->description . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}


function translate_woocommerce($translation, $text, $domain) {
    if ($domain == 'woocommerce') {
        switch ($text) {
            case 'SKU':
                $translation = 'Item Number';
                break;
            case 'SKU:':
                $translation = 'Item Number:';
                break;
        }
    }
    return $translation;
}

add_filter('gettext', 'translate_woocommerce', 10, 3);

/*Remove stock and type from admin*/
add_filter('gettext', 'translate_woocommerce', 10, 3);

add_filter( 'gform_field_value_item_number', 'item_number_function' );
function item_number_function( $value ) {
  global $post;
    return get_post_meta( $post->ID, 'product_sku', true );
}

add_filter( 'manage_edit-product_columns', 'change_columns_filter',10, 1 );
function change_columns_filter( $columns ) {
$columns['HotBuy'] = __( 'Hot Buy');
$columns['PriceRange'] = __( 'Price Range');
$columns['quantity'] = __( 'Quantity');
$columns['displayWeb'] = __( 'Display On Website');
unset($columns['product_tag']);
unset($columns['is_in_stock']);
unset($columns['product_type']);
unset($columns['price']);
unset($columns['wpseo-links']);
unset($columns['wpseo-score']);
unset($columns['wpseo-score-readability']); 
return $columns;
}

add_action( 'manage_product_posts_custom_column', 'column_pricerange', 10, 2 );
function font_styles()  
{
wp_enqueue_style( 'customfonticons', get_template_directory_uri() . '/css/customfonticons.css' ); 
}
add_action('wp_enqueue_scripts', 'font_styles');
function column_pricerange($column, $postid) {
	 if ( $column == 'HotBuy' ) {
        $HotBuy = get_post_meta( $postid, 'hot_buy', true );
        if($HotBuy == 1){echo '<a title="Yes" href="'.get_site_url().'/wp-admin/admin-ajax.php?action=woocommerce_hotbuy&product_id='.$postid.'&hotbuy_val=0"><span class="wc-hotbuy">Yes</span></a>';}else{ echo '<a title="No" href="'.get_site_url().'/wp-admin/admin-ajax.php?action=woocommerce_hotbuy&product_id='.$postid.'&hotbuy_val=1"><span class="wc-hotbuy not-hotbuy">No</span></a>'; }
    }
    if ( $column == 'PriceRange' ) {
        echo get_post_meta( $postid, 'price_range', true );
    }
     if ( $column == 'quantity' ) {
        echo get_post_meta( $postid, 'quantity', true );
    }
    if ( $column == 'displayWeb' ) {
        $visibility = get_post_meta( $postid, 'product_visible', true );        
        if($visibility == 1){echo '<a title="Hidden" href="'.get_site_url().'/wp-admin/admin-ajax.php?action=woocommerce_visibility&product_id='.$postid.'&visibility_val=0"><span class="wc-displayweb">Hidden</span></a>';}else{ echo '<a title="Visible" href="'.get_site_url().'/wp-admin/admin-ajax.php?action=woocommerce_visibility&product_id='.$postid.'&visibility_val=1"><span class="wc-displayweb yes-displayweb">Visible</span></a>'; }
    }
   
}
// Display on website 
add_action( 'wp_ajax_woocommerce_visibility', 'woocommerce_visibility' );
add_action( 'wp_ajax_nopriv_woocommerce_visibility', 'woocommerce_visibility' );
function woocommerce_visibility(){
  $product_id = $_GET['product_id'];
  $visibility_val = $_GET['visibility_val'];
  //update_field( 'product_visible', $_GET['visibility_val'], $_GET['product_id'] );
  update_post_meta( $product_id, 'product_visible', $visibility_val );
  wp_safe_redirect( wp_get_referer() ? remove_query_arg( array( 'trashed', 'untrashed', 'deleted', 'ids' ), wp_get_referer() ) : admin_url( 'edit.php?post_type=product' ) );
  exit;

}
add_action('woocommerce_product_query','shop_filter');
function shop_filter($query) {  
      $query->set('meta_query', array(
                  array(
                      'key'       => 'product_visible',
                      'value'     => '0',
                      'compare'   => '==',
                      )
                    )
                  );   
    
  }
/*$product_metas = array(6496, 6666, 6753, 6423, 6424, 6425, 6426, 6427, 6431, 6432, 6433, 6434, 6441, 6442, 6443, 6444, 6445, 6446, 6447, 6448, 6450, 6451, 6452, 6453, 6454, 6455, 6456, 6457, 6458, 6459, 6460, 6461, 6462, 6463, 6465, 6472, 6474, 6475, 6477, 6481, 6484, 6485, 6486, 6490, 6504, 6519, 6527, 6533, 6540, 6547, 6559, 6567, 6576, 6594, 6600, 6615, 6622, 6634, 6640, 6647, 6659, 6695, 6701, 6716, 6723, 6726, 6741, 6747, 6813, 6824, 6830, 6856, 6873, 6882, 6887, 6893, 6905, 6920, 6927, 6931, 6936, 6941, 6946, 6955, 7012, 7018, 7024, 7040, 7044, 7048, 7060, 7066, 7081, 7088, 7101, 7110, 7124, 7128, 7135, 7138, 7153, 7172, 7181, 7188, 7194, 7201, 7206, 7216, 7220, 7227, 7234, 7241, 7249, 7256, 7273, 7283, 7295, 7337, 7342, 7349, 7354, 7361, 7371, 7378, 7387, 7398, 7402, 7406, 7415, 7424, 7433, 7444, 7459, 7464, 7469, 7475, 7479, 7487, 7493, 6435, 6476, 6483, 6489, 6554, 6571, 6584, 6605, 6611, 6628, 6653, 6683, 6710, 6862, 6951, 7053, 7074, 7097, 7102, 7145, 7162, 7332, 7445);
foreach ($product_metas as $id) {
  update_post_meta( $id, 'by_appointment_sel', 'No' );
}*/

// Hot buy CSS
function load_custom_wp_admin_admin_product() {
        wp_register_style( 'admin-product', get_template_directory_uri() . '/css/admin-product.css', false, '1.0.0' );
        wp_enqueue_style( 'admin-product' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_admin_product' );
// Hot buy 
add_action( 'wp_ajax_woocommerce_hotbuy', 'woocommerce_hotbuy' );
add_action( 'wp_ajax_nopriv_woocommerce_hotbuy', 'woocommerce_hotbuy' );
function woocommerce_hotbuy(){
  $product_id = $_GET['product_id'];
  $hotbuy_val = $_GET['hotbuy_val'];
  update_field( 'hot_buy', $hotbuy_val, $product_id );
  wp_safe_redirect( wp_get_referer() ? remove_query_arg( array( 'trashed', 'untrashed', 'deleted', 'ids' ), wp_get_referer() ) : admin_url( 'edit.php?post_type=product' ) );
  exit;

}
//Remove product data tab from admin
add_action('admin_head', 'admin_custom_css');

function admin_custom_css() {
  echo '<style>
    
  </style>';
}

// Add hot buy class
function hotbuy_post_class ( $classes ) {
   global $post;   
   $hot_buy = get_field('hot_buy', $post->ID);
   if($hot_buy == 1 ){
   		$current_class = 'product-hot-buy';
	}else{
		$current_class = '';
	}
	$classes[] = $current_class;
   //$current_class = ($current_class == 'odd') ? 'even' : 'odd';
   return $classes;
}
add_filter ( 'post_class' , 'hotbuy_post_class' );
// Search page class
add_filter( 'body_class', 'search_body_class');
function search_body_class( $classes ) {
     if(is_search() || is_page_template('template_featured.php')){
          $classes[] = 'woocommerce';
        }
     return $classes;
}
// search restrict for product
function searchfilter($query) {
  //$s = $_GET['s'];
  //print_r($query->query_vars['s']);
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('product'));
         $meta_query_args = array(
                  'relation' => 'OR',
                  array(
                      'key'       => 'product_visible',
                      'value'     => '0',
                      'compare'   => '==',
                      ),
                  /*array(
                      'key'       => 'product_sku',
                      'value'     => $query->query_vars['s'],
                      'compare'   => 'LIKE',
                      )*/
                    );
        $query->set('meta_query', $meta_query_args );        
        /*$query->set('meta_query', array(
                  array(
                      'key'       => 'product_sku',
                      'value'     => $s,
                      'compare'   => 'LIKE',
                      )
                    )
                  );*/
        /*echo "<pre>";
        print_r($query);
        echo "</pre>";*/
    }

return $query;
}
add_filter('pre_get_posts','searchfilter');

// Add attribute to garvity form and fields.
add_filter("gform_field_content", "ml_gravityforms_fields", 10, 5);
function ml_gravityforms_fields($content, $field, $value, $lead_id, $form_id){  
  // Currently only applies to most common field types, but could be expanded.
 
  if($field["type"] == 'text' || $field["type"] == 'email' || $field["type"] == 'phone' || $field["type"] == 'date' || $field["type"] == 'hidden' ) {
    $label = $field["label"];
    $content = str_replace('<input ', '<input data-ml-field="'.$label.'" ', $content);
  }
  if($field["type"] == 'textarea') {
    $label = $field["label"];
    $content = str_replace('<textarea ', '<textarea data-ml-field="'.$label.'" ', $content);
  }
  if($field["type"] == 'radio') {
    $label = $field["label"];
    $content = str_replace('<input ', '<input data-ml-field="'.$label.'" ', $content);
  } 
  
  return $content;
  
} 

add_filter( 'gform_form_tag', 'ml_garvity_form_tag', 10, 2 );
function ml_garvity_form_tag( $form_tag, $form ) {    
    $formID = $form['id'];
    $forminfo = RGFormsModel::get_form($formID);
    $form_title = $forminfo->title;
    $form_tag = str_replace( "<form", '<form data-ml-form="'.$form_title.'" ', $form_tag );
    return $form_tag;
}
// End garvity form and fields function.

// Contact for validation
add_filter( 'gform_validation_1', 'contact_validation' );
function contact_validation($validation_result) {
  $form = $validation_result['form'];

    //supposing we don't want input 1 to be a value of 86
    if ( rgpost( 'input_2' ) == '' && rgpost( 'input_3' ) == '') {

        // set the form validation to false
        $validation_result['is_valid'] = false;

        //finding Field with ID of 1 and marking it as failed validation
        foreach( $form['fields'] as &$field ) {

            //NOTE: replace 1 with the field you would like to validate
            if ( $field->id == '2') {
                $field->failed_validation = true;
                $field->validation_message = 'Please enter either phone or email.';
            }
            if ( $field->id == '3') {
                $field->failed_validation = true;
                $field->validation_message = 'Please enter either phone or email.';
                break;
            }
        }

}
//Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;
}

// Add product category in admin
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
  global $typenow;
  $post_type = 'product'; // change to your post type
  $taxonomy  = 'product_cat'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET['cat_product']) ? $_GET['cat_product'] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Filter by category"),
      'show_option_none' => 'All Products',
      'option_none_value' => 0,
      'taxonomy'        => $taxonomy,
      'name'            => 'cat_product',
      'orderby'         => 'name',
      'selected'        => $selected,
      'hierarchical'    =>  true,
      'depth'           =>  3,
      'value_field'     => 'slug',
      'show_count'      => false,
      'hide_empty'      => true,
    ));
  };
}

add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
  global $pagenow;
  $post_type = 'product'; // change to your post type
  $taxonomy  = 'product_cat'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
//print_r($q_vars);
  if ( is_admin() && $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && $_GET['cat_product']) {
    /*$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);*/
    $term = $_GET['cat_product'];
    $q_vars[$taxonomy] = $term;
  }
}
add_action( 'admin_init', 'bb_remove_yoast_seo_admin_filters', 20 );
function bb_remove_yoast_seo_admin_filters() {
    global $wpseo_meta_columns ;
    if ( $wpseo_meta_columns  ) {
        remove_action( 'restrict_manage_posts', array( $wpseo_meta_columns , 'posts_filter_dropdown' ) );
        remove_action( 'restrict_manage_posts', array( $wpseo_meta_columns , 'posts_filter_dropdown_readability' ) );
    }
}
/*========== Remove Category Column on Wordpress Category Listing Page ==========*/
function cap_remove_category_desc_cat_listing($columns)
{
// only edit the columns on the current taxonomy
if ( !isset($_GET['taxonomy']) || ($_GET['taxonomy'] != 'category' && $_GET['taxonomy'] != 'product_cat') || ($_GET['taxonomy'] != 'category' && $_GET['taxonomy'] != 'product_tag'))
    return $columns;

// unset the description columns
if ( $posts = $columns['description'] ){ unset($columns['description']); }

return $columns;
}
add_filter('manage_edit-category_columns','cap_remove_category_desc_cat_listing');
add_filter('manage_edit-product_cat_columns','cap_remove_category_desc_cat_listing');
/*Add class to Toni's detail page*/
function add_toni_class_single( $classes ){
  global $post;
  if(is_singular('product')){
    $term_list = wp_get_post_terms($post->ID, 'product_cat', array("fields" => "ids"));
    foreach ($term_list as $value) {
      $child_term = get_term( $value, 'product_cat' );
      $parent_term = get_term( $child_term->parent, 'product_cat' );
      //print_r($parent_term);
      $parent = $parent_term->term_id;
      if($value == '748' || $parent == '748')
      $classes[] = 'product_cat-tonis-treasures';
    }
    
    //print_r($post_categories);
  }
  return $classes;
}
add_filter('post_class','add_toni_class_single');
// Featured shortcode
function FeaturedProduct_func( $atts ) {
    $args = array( 
            'post_type' => 'product',
            'post_status'      => 'publish',
            'posts_per_page' => 50,
            'meta_query' => array(
                  array(
                      'key'       => 'product_visible',
                      'value'     => '0',
                      'compare'   => '==',
                      ),
                    ),
            'tax_query' => array(
                    array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                    ),
                ),             
        );
    query_posts( $args );
    if ( have_posts() ) :
        $out = '<div class="cycle-carousel-wrap">'; 
    while ( have_posts() ) : the_post();
        $featured_img_url = get_the_post_thumbnail_url($post->ID,'full');
        $props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
        $image            = get_the_post_thumbnail( $post->ID, 'full', array(
                'title'  => $props['title'],
                'alt'    => $props['alt'],                
            ) );
        $out .= '<li class="' . join( ' ', get_post_class( "cycle-slide", $post->ID ) ) . '"><a href="'.get_permalink( $post->ID ).'" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                '.$image.'
            <h2 class="woocommerce-loop-product__title">'.get_the_title().'</h2>
            </a>            
            </li>';
    endwhile;
        $out .= '</div>';
    endif;
    wp_reset_postdata();

    return $out;
}
add_shortcode( 'FeaturedProduct', 'FeaturedProduct_func' );

// Post Slider shortcode
/*function PostSlider_func( $atts ) {
    $args = array( 
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 10,
        );
    query_posts( $args );
    if ( have_posts() ) :
        $out = '<ul class="cycle-carousel-wrap imple-blog small-block-grid-1 medium-block-grid-2 large-block-grid-3">'; 
    while ( have_posts() ) : the_post();
        $featured_img_url = get_the_post_thumbnail_url($post->ID,'full');
        $props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
        $image            = get_the_post_thumbnail( $post->ID, 'full', array(
                'title'  => $props['title'],
                'alt'    => $props['alt'],                
            ) );
        $out .= '<li class="cycle-slide">
                <div class="wd-latest-news hvr-underline-from-center  animated  fadeInUp" data-animated="fadeInUp" style="opacity: 1; animation-delay: 0ms;">
                  <div class="wd-image-date">
                  '.$image.'
                  </div>
                  <h4 class="wd-title-element">
                    <a href="'.get_permalink( $post->ID ).'" target="_blank">'.get_the_title().'</a>
                  </h4>                                
                  <p>
                    Carpets will be among the most expensive items to purchase when furnishing your house, business or office. So naturally you’ll have to preserve the quality of your carpets to make sure their lifetime expectancy. Carpets work as the primary...<a class="hvr-pop read-more" href="'.get_permalink( $post->ID ).'" target="_blank">Read More →</a>
                  </p>
                </div>            
            </li>';
    endwhile;
        $out .= '</ul>';
    endif;
    wp_reset_postdata();

    return $out;
}
add_shortcode( 'PostSlider', 'PostSlider_func' );*/
?>