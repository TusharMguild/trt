<?php

get_header();

	echo '<section id="content">';

		if ( is_front_page() ) {
			get_template_part( 'inc/chunk' , 'slider' ); // Slider

		} else {
			get_template_part( 'inc/chunk' , 'title' ); // The Title
		}

		echo (	'<div class="page-wrapper" id="main-wrapper">'.
				'<main role="main" id="main-content" class="clearfix">');					

					get_template_part( 'inc/chunk' , 'content' ); // The Content

					Global $postType;
					if ( $postType != 'page' ) { get_template_part( 'inc/chunk' , 'pagination' );}else{} // Pagination

			echo '</main>';
			# get_sidebar();
	echo ('</div>'.
	'</section>');

get_footer();

?>