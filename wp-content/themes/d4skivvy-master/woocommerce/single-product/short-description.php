<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

if ( ! $post->post_excerpt ) {
	return;
}

?>
<?php

$tr_price = get_post_meta( $post->ID, 'Price Range', true );
if ($tr_price == 'Call us') {
	$tr_price = '<a id="call-us-pricing" style="cursor:pointer">Call us about pricing</a>';
}

?>
<div id="single-short-desc" itemprop="description">
	<ul id="product-meta-list">
	<li>Item: #<?php echo get_post_meta( $post->ID, 'product_sku', true );?></li>	
	<li>Quantity: <?php echo get_field('quantity', $post->ID);?></li>
	<li><?php echo get_field('product_condition', $post->ID);?></li>
	</ul>
	<h3 class="priceh3">Price: <?php echo get_field('price_range', $post->ID);?></h3>
	<div id="inquire-button">Inquire About This Product</div>
	
	<div id="single-features">
		<h3>Features</h3>
		<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
	</div>	
</div>
