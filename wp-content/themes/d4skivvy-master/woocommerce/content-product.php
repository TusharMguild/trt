<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php post_class(); ?>>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_item_title' );
	$attachment_id = get_post_thumbnail_id( $post->ID );
	//echo $string = wc_placeholder_img_src()
	?>
	<a href="<?php echo get_permalink($post->ID);?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
	<?php $product_img = wp_get_attachment_image( $attachment_id, 'full' );;
		if(!empty($product_img)){
			echo $product_img;
		}else{
			echo '<img src="'.wc_placeholder_img_src().'" alt="Placeholder"  class="woocommerce-placeholder wp-post-image">';
		}
	 ?>
	<h2 class="woocommerce-loop-product__title"><?php echo get_the_title($post->ID);?></h2>
</a>
	<?php
	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
	<?php
	$appointmentOnly = get_post_meta( $post->ID, 'by_appointment_sel', true ); 
	if($appointmentOnly == 'Yes') { echo '<p class="appointment-only">Shown By Appointment Only</p>';} ?>
	<h3><?php echo get_post_meta( $post->ID, 'Price Range', true );?></h3>
</li>
