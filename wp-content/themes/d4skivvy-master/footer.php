<div id="modalform"><div id="modaloverlay"></div><div class="page-wrapper"><i class="icon-close"></i><div class="modalinner">
<?php echo '<h3 style="text-align:center">'.get_the_title(get_the_ID()).'</h3>';?>
<p style="text-align:center">Item: #<?php echo get_post_meta( $post->ID, 'product_sku', true );?></p>
<?php 
$term_list = wp_get_post_terms($post->ID, 'product_cat', array("fields" => "ids"));
//print_r($term_list);
if (in_array("748", $term_list)) {
  echo do_shortcode('[gravityform id="4" title="true" description="true" ajax="true"]');
}else{
echo do_shortcode('[gravityform id="2" title="true" description="true" ajax="true"]');
}
?>
</div></div></div>
<footer id="footer"><?php
	
$contact_footer_page = get_post(29);
echo do_shortcode(wpautop($contact_footer_page->post_content));


		echo '<div role="contentinfo" id="footer-copyright" class="textcenter">';
			$start_year = "2015"; // Starting Year
			$date = date( 'Y' ); // Current year
			if ( $date > $start_year ) $date = "$start_year - $date"; // If the two years don't match, they go to 'n fro. If you don't want the hyphenated date, comment out this line.

			_e( sprintf( 'Copyright &copy; %1$s %4$s. All Rights Reserved. ' , $date, home_url('/'), esc_attr( get_bloginfo( 'name', 'display' ) ), get_bloginfo( 'name' )), 'skivvy' );

			
		echo '</div>';


	echo '</div>';
	wp_footer();
?></footer>

<script type="text/javascript">
  (function() {
    var mgElement = document.createElement('script'); 
    mgElement.type = 'text/javascript'; mgElement.async = true;
    mgElement.src = '//s3.amazonaws.com/metroleads/quasar.min.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mgElement, s);

    // Handle Script loading
    var done = false;
    var head = document.getElementsByTagName("head")[0] || document.documentElement;
    mgElement.onload = mgElement.onreadystatechange = function() {
      if ( !done && (!this.readyState ||
              this.readyState === "loaded" || this.readyState === "complete") ) {
          done = true;
          mg = new MG({}, '32b962da-4129-11e3-ad07-22000a989eb8', '1b081f0e-e6c5-46fa-8850-4d136e1cd5a2');
          mg.init();

          // Handle memory leak in IE
          mgElement.onload = mgElement.onreadystatechange = null;
          if ( head && mgElement.parentNode == head ) {
              head.removeChild( mgElement );
          }
      }
    };
  })();
</script>
</body>
</html>