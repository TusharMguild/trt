<!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php if (function_exists('html_classes')){ html_classes(); } ?>">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23158120-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23158120-1');
</script>

	<?php

	echo (
			'<meta charset="'. get_bloginfo( 'charset' ).'">'.
			'<meta content="' . get_bloginfo( 'description', 'display' ) . '" name="description">'. // Meta description, important for SEO. Defaults to blog's description.
			//'<meta content="" name="keywords">'.
			'<meta content="width=device-width, initial-scale=1.0" name="viewport">'. // Sets default width and scale to be dependent on the device.
			'<link rel="shortcut icon" type="image/png" href="' . get_stylesheet_directory_uri() . '/img/favicon.png?v=1">'.
		#	'<link rel="apple-touch-icon" href="' . get_stylesheet_directory_uri() . '/apple-touch-icon.png">'.


		// Paste Google Fonts here
			"<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic,800' rel='stylesheet' type='text/css'>"

	);

	wp_head();
?></head>
<body id="page-<?php the_ID(); ?>" <?php body_class(); ?>>
<header role="banner" id="header">
	<?php
	Global $postType;
	$postType = get_post_type();
	$walker = new Menu_With_Description;

		echo (
			// Logo
				'<a id="logo" class="alignleft" href="' . home_url( '/' ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" rel="home">' .
					'<img src="' . get_stylesheet_directory_uri() . '/img/logo.png" alt="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '">' .
				'</a>'.
				
				'<div class="cartoon"><img src="' . get_stylesheet_directory_uri() . '/img/trt_cartoon.jpg"></div>'.

			// Mobile Toggle
				'<button id="mobile-toggle" class="alignright dont-print icon-bars"></button>'.

			// Main Menu
				'<nav role="navigation" class="dont-print">'.
						wp_nav_menu( array(
							'container'       => false,
							'menu_class'      => 'justified nobull dropdown animated flyoutright', // Menu functionality classes
							'menu_id'         => 'main-menu',
							'theme_location'  => 'main',
							'depth'           => 3, // 0 = all. Default, -1 = displays links at any depth and arranges them in a single, flat list.
							'echo'            => false,
							'walker'		  => $walker
						)).

					'<div class="clear"></div>'.
					
				'</nav>'				
		);

	?>
	<div class="search-mobile"><?php get_search_form(); ?></div>
</header>
<?php 
echo '<div id="mobile-overlay"></div>';
// Mobile Menu
wp_nav_menu( array(
	'container'       => false,
	'menu_class'      => 'nobull textright clearfix',
	'menu_id'         => 'mobile-nav',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'theme_location'  => 'mobile',
	'depth'           => 2,
	'echo'            => true
));

echo '<div id="header-contact">';
?>
<?php get_search_form(); ?>
<?php
echo '<a id="header-contact-button" href="/contact-us/">Contact Us</a><a id="header-contact-button" href="/liquidation-services/">Liquidate</a>';
?>
<ul class="socialbox  social_text social_link">
	<li class="socialitem_1 socialbox_phone1 socialbox_phone">
		<a href="/contact-us/" title="Contact Us">
			<i class="icon-envelope"></i> Contact Us
		</a>
	</li>
</ul>
<?php
		//do_shortcode('[socialbox key="phone1" style="link"]');
echo '</div>';
