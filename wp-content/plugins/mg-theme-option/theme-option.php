<?php
/**
* Plugin Name: MG Theme Option
* Description: This plugin use to manage theme global setting and pages content.
* Version: 1.0
* Author: MetroGuild
* Author URI: http://www.metroguild.com
*/

 /**
  * Exit if this wasn't accessed via WordPress (aka via direct access)
  */
if (!defined('ABSPATH')) exit;
/**
* Create base class for Plugin.
*/
if ( !(class_exists('ThemeOption')) ) {
class ThemeOption {
	/**
    * Add appropriate actions.
    */
	public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_theme_menu' ) );
        add_action('admin_enqueue_scripts', array( $this, 'enqueue_media'));
        add_action( 'init', array( $this,'register_special_shortcodes'));
	}

    /**
    * Add theme setting page
    */
    public function add_theme_menu($attr) {             
        add_menu_page(
            'Theme Panel', 
            'Theme Panel', 
            'manage_options', 
            'theme-panel', 
            array( $this, 'theme_settings_page' ), 
            null, 
            99
        );
    }    
    /**
    * Theme setting page callback
    */
    public function theme_settings_page($current = 'homepage') {
        global $pagenow;
        $tabs = array( 'homepage' => 'Home Settings');
    echo '<div class="wrap"><div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=theme-panel&tab=$tab'>$name</a>";

    }
    echo '</h2></div>';

    if ( $pagenow == 'admin.php' && $_GET['page'] == 'theme-panel' ) :
        if ( isset ( $_GET['tab'] ) ) :
            $tab = $_GET['tab'];
        else:
            $tab = 'homepage';
        endif;
        switch ( $tab ) :
            case 'homepage' :
                $this->theme_home_options();
                break;
            
        endswitch;
    endif;
    }
    /**
    * Theme home setting page callback
    */
    public function theme_home_options(){

        ?>
    <div class="wrap">
        <?php 
            //print_r($_POST);
            if (isset($_POST['Submit'])) {
                $deprecated = null;
                $autoload = 'no';
                // For SpecialText
                if(($_POST['SpecialText'] != '')){
                    if ( (get_option( 'SpecialText' ) !== false)){
                        update_option( 'SpecialText', $_POST['SpecialText'] );
                    }else{
                        add_option( 'SpecialText', $_POST['SpecialText'], $deprecated, $autoload );
                    }
                }
                // End SpecialText
                // For SpecialButton
                if(($_POST['SpecialButton'] != '')){
                    if ( (get_option( 'SpecialButton' ) !== false)){
                        update_option( 'SpecialButton', $_POST['SpecialButton'] );
                    }else{
                        add_option( 'SpecialButton', $_POST['SpecialButton'], $deprecated, $autoload );
                    }
                }
                // End SpecialButton
                // For SpecialButtonLink
                if(($_POST['SpecialButtonLink'] != '')){
                    if ( (get_option( 'SpecialButtonLink' ) !== false)){
                        update_option( 'SpecialButtonLink', $_POST['SpecialButtonLink'] );
                    }else{
                        add_option( 'SpecialButtonLink', $_POST['SpecialButtonLink'], $deprecated, $autoload );
                    }
                }
                // End SpecialButtonLink
                // For SpecialImageUrl
                if(($_POST['SpecialImageUrl'] != '')){
                    if ( (get_option( 'SpecialImageUrl' ) !== false)){
                        update_option( 'SpecialImageUrl', $_POST['SpecialImageUrl'] );
                    }else{
                        add_option( 'SpecialImageUrl', $_POST['SpecialImageUrl'], $deprecated, $autoload );
                    }
                }
                // End SpecialText
            }
        ?>
        <h2>Area for Specials</h2>
        <form method="post" action="" enctype="multipart/form-data">
            <p><strong>Special Text</strong><br />
                <input type="text" name="SpecialText" size="45" value="<?php echo $SpecialText = (get_option( 'SpecialText' ) !== false) ? get_option( 'SpecialText' ) : '' ;?>" />
            </p>
            <p><strong>Special Button Text:</strong><br />
                <input type="text" name="SpecialButton" size="45" value="<?php echo $SpecialButton = (get_option( 'SpecialButton' ) !== false) ? get_option( 'SpecialButton' ) : '' ;?>" />
            </p>
            <p><strong>Special Button Link:</strong><br />
                <input type="text" name="SpecialButtonLink" size="45" value="<?php echo $SpecialButtonLink = (get_option( 'SpecialButtonLink' ) !== false) ? get_option( 'SpecialButtonLink' ) : '' ;?>" />
            </p>
            <p><strong>Special Image:</strong><br />
                <input class="SpecialImageUrl" type="text" name="SpecialImageUrl" size="45" value="<?php echo $SpecialImageUrl = (get_option( 'SpecialImageUrl' ) !== false) ? get_option( 'SpecialImageUrl' ) : '' ;?>"><button class="SpecialImageUpload">Upload</button> <br /><br/>
                <?php $SpecialImage = get_option('SpecialImageUrl');
                if(!empty($SpecialImage)){ ?>
                <img class="SpecialImage" src="<?php echo get_option('SpecialImageUrl'); ?>" height="200" width="311"/>
                <?php } ?>
            </p> 
            <p><input type="submit" name="Submit" value="Submit" /></p>
        </form>
        <div class="acf-columns">Please add below shortcode inside the page or post editor <br /> <strong>[SpecialArea]</strong></div>
        <script>
    jQuery(document).ready(function($) {
        $('.SpecialImageUpload').click(function(e) {
            e.preventDefault();

            var custom_uploader = wp.media({
                title: 'Special ImageUp',
                button: {
                    text: 'Upload Image'
                },
                multiple: false  // Set this to true to allow multiple files to be selected
            })
            .on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $('.SpecialImage').attr('src', attachment.url);
                $('.SpecialImageUrl').val(attachment.url);

            })
            .open();
        });
    });
</script>
    </div>
<?php
    }
    /**
    * Add media library
    */
    public function enqueue_media() {
        if( function_exists( 'wp_enqueue_media' ) ) {
            wp_enqueue_media();
        }
    }
    /**
    * Create shortcode for special area
    */
    public function register_special_shortcodes(){
        add_shortcode( 'SpecialArea', array($this,'specialarea_shortcode'));
    }
    public function specialarea_shortcode(){
        $SpecialImageUrl = get_option('SpecialImageUrl');
        if(empty($SpecialImageUrlar)){
            $SpecialArea ='<div id="product-showcase" class="fullwidth">'
            .'<div class="skivdiv-content">'
            .'<p><img class="alignnone size-full wp-image-26" src="'.$SpecialImageUrl.'" alt=""></p>'
            .'<h1>'.get_option( 'SpecialText' ).'</h1>'
            .'<p><a href="'.get_option( 'SpecialButtonLink' ).'" class="button"><i class="button-icon "></i><span class="button-text">'.get_option( 'SpecialButton' ).'</span></a></p>'
            .'<div class="clear"></div>'
            .'</div>'
            .'</div>';
            return $SpecialArea;
        }else{
            return '';
        }

    }    
    
}
new ThemeOption();
}