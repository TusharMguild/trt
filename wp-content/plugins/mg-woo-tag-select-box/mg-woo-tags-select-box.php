<?php
/**
* Plugin Name: Woo Tags Meta Box
* Description: This plugin help you to add meta box into product and quick edit.
* Version: 1.0
* Author: MetroGuild
* Author URI: http://www.metroguild.com
*/

 /**
  * Exit if this wasn't accessed via WordPress (aka via direct access)
  */
if (!defined('ABSPATH')) exit;
// Hide Plugin from Network plugins page to prevent network activation of plugin.
/*function ml_hide_network_plugin( $all ) {
    global $current_screen;

    if( $current_screen->is_network ) {
        unset($all['ml-woo-add-column/woo-custom-column.php']);
    }
    return $all;
}
add_filter( 'all_plugins', 'ml_hide_network_plugin' );*/
/**
* Create base class for Plugin.
*/
if ( !(class_exists('WooTagsSelectBox')) ) {
class WooTagsSelectBox
{
	/**
    * Add appropriate actions.
    */
	public function __construct() {
        global $QuickEditFields, $post, $post_id;
        /*add_action( 'admin_menu', array( $this, 'add_quick_edit_menu' ) );
        add_action( 'quick_edit_custom_box', array( $this, 'quantity_quickedit_fields' ), 10, 2 );
        add_action( 'save_post', array( $this, 'quantity_quickedit_save_post' ), 10, 2 );
        add_action( 'admin_print_footer_scripts-edit.php', array( $this, 'quantity_quickedit_javascript' ) );
        add_filter('post_row_actions', array( $this, 'quantity_quickedit_set_data' ), 10, 2);*/
        /*add_action( 'admin_menu', array( $this, 'add_theme_menu' ) );
        add_action( 'wp_enqueue_scripts', array( &$this,'PostResponsiveCarousel') );
        add_action( 'init', array( $this,'register_special_shortcodes') );
        add_action('wp_footer', array( $this,'LightBoxDiv') );*/
        add_action( 'admin_menu', array( $this, 'woo_product_tags_meta_box_remove') );
        add_action( 'admin_menu', array( $this, 'add_woo_product_tags_metabox') );
        add_action('save_post', array( $this, 'woo_save_postdata') );
        // Quick edit
        add_action( 'quick_edit_custom_box', array( $this, 'woo_tag_quickedit_fields' ), 10, 2 );
        add_action( 'admin_print_footer_scripts-edit.php', array( $this, 'woo_quickedit_javascript' ) );
        add_filter('post_row_actions', array( $this, 'woo_quickedit_set_data' ), 10, 2);
	}
    /*
     * Meta Box Removal
     */
    public function woo_product_tags_meta_box_remove() {
        $id = 'tagsdiv-product_tag'; // you can find it in a page source code (Ctrl+U)
        $post_type = 'product'; // remove only from post edit screen
        $position = 'side';
        remove_meta_box( $id, $post_type, $position );
    }
    /*
     * Add
     */
    public function add_woo_product_tags_metabox(){
        $id = 'mgwootagsdiv-product_tag'; // it should be unique
        $heading = 'Product Tags'; // meta box heading
        $callback = array( $this, 'mg_metabox_content'); // the name of the callback function
        $post_type = 'product';
        $position = 'side';
        $pri = 'default'; // priority, 'default' is good for us
        add_meta_box( $id, $heading, $callback, $post_type, $position, $pri );
    }
    /*
     * Fill
     */
    function mg_metabox_content($post) {
        /*global $post;
        echo ">>>>>>>";  
        print_r($post);*/
        wp_nonce_field( 'mgwoo_tag_metabox_nonce', 'mgwoo_tag_nonce' );
        // get all blog post tags as an array of objects
        $all_tags = get_terms( array('taxonomy' => 'product_tag', 'hide_empty' => 0) );
        //print_r($all_tags); 
     
        // get all tags assigned to a post
        $all_tags_of_product = get_the_terms( $post->ID, 'product_tag' );

        $term_list = wp_get_post_terms($post->ID, 'product_tag', array("fields" => "ids"));
        //print_r($term_list[0]);  
     
        // create an array of post tags ids
        $ids = array();
        if ( $all_tags_of_product ) {
            foreach ($all_tags_of_product as $tag ) {
                $ids[] = $tag->term_id;
            }
        }
     
        // HTML
        echo '<div id="taxonomy-product_tag" class="categorydiv">';
        echo "<select name='mgwootag'>";
        echo "<option value=''>--Select Tag--</option>";
        foreach( $all_tags as $tag ){
            $selected = ( $term_list[0] == $tag->term_id ) ? ' selected' : '';            
            echo "<option value='$tag->term_id' $selected >$tag->name</option>";
        }
        echo "</select>";
        echo '</div>'; // end HTML
    }
    /**
    * Add field to quick edit
    */
    public function woo_tag_quickedit_fields($column_name, $post_type){
        if ( 'product_tag' != $column_name )
            return;
        $all_tags = get_terms( array('taxonomy' => 'product_tag', 'hide_empty' => 0) );
        ?>
        <fieldset class="inline-edit-col-left">
            <div class="inline-edit-col">
                <label>
                    <span class="title"><?php esc_html_e( 'Product Tag', 'product_tag' ); ?></span>
                    <span class="input-text-wrap">
                        <input type="hidden" name="woo_product_tag_noncename" id="woo_product_tag_noncename" value="" />
                        <select name="woo_product_tag_edit" class="woo_product_tag" id="woo_product_tag">
                            <option class='product-tag-option' value="">Select Tag</option>
                            <?php foreach( $all_tags as $tag ){ 
                                ?>
                                <option class='product-tag-option' value="<?php echo $tag->name; ?>"><?php echo $tag->name; ?></option>
                                <?php
                            }?>                            
                        </select>
                    </span>
                </label>
            </div>
        </fieldset>
        <?php
    }
    /**
    * Save product tag data
    */
    public function woo_save_postdata($post_id){

        // if called by autosave, then bail here
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return;

        // if this "post" post type?
        /*if ( $post->post_type != 'product' )
            return;*/

        // does this user have permissions?
        if ( !current_user_can( 'edit_post', $post_id ))
        return;

        // Single product
        if ( isset($_POST['mgwootag']) ) {
            $term = get_term( $_POST['mgwootag'], 'product_tag' );
            wp_set_post_terms( $post_id, $term->name, 'product_tag');
        }

        // Product quick edit
        if ( isset( $_POST['woo_product_tag_edit'] ) ) {
          //$term = get_term( $_POST['woo_product_tag_edit'], 'product_tag' );
          wp_set_post_terms( $post_id, $_POST['woo_product_tag_edit'], 'product_tag');
        }
    }
    /**
    * Add script for quick edit tag field
    */
    public function woo_quickedit_javascript() {
            $current_screen = get_current_screen();
           //print_r($current_screen);
            if ( $current_screen->id != 'edit-product' || $current_screen->post_type != 'product' )
                return;

            // Ensure jQuery library loads
            wp_enqueue_script( 'jquery' );
            ?>
            <script type="text/javascript">
                jQuery( function( $ ) {
                    $( '#the-list' ).on( 'click', 'a.editinline', function( e ) {               
                        e.preventDefault();
                        var woo_product_tag_val = $(this).data( 'woo_product_tag' );
                        inlineEditPost.revert();
                        //debugger;
                        //alert(woo_product_tag_val);
                        $("#woo_product_tag option").each(function()
                        {
                            if($(this).val() == woo_product_tag_val){
                                $(this).attr('selected', true);
                            } else {
                                $(this).attr('selected', false);
                            }
                        });
                        $( '.woo_product_tag' ).val( woo_product_tag_val ? woo_product_tag_val : '' );
                    });
                });
            </script>
    <?php
    }

    /**
    * Set value for product tag quick edit field
    */
    public function woo_quickedit_set_data( $actions, $post ) {
        //echo ">>>".$post->ID;
        $product_tag = wp_get_post_terms($post->ID, 'product_tag', array("fields" => "names"));
        /*print_r($product_tag);
        exit();*/
        $woo_product_tag = $product_tag[0];       
        if ( $woo_product_tag ) {
            if ( isset( $actions['inline hide-if-no-js'] ) ) {
                $new_attribute = sprintf( 'data-woo_product_tag="%s"', esc_attr( $woo_product_tag ) );
                $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
            }
        }
        return $actions;
    }
    /**
    * Add theme setting page
    */
    /*public function add_quick_edit_menu($attr) {             
        add_menu_page(
            'Woo  Edit Panel', 
            'Quick Edit Panel', 
            'manage_options', 
            'quick-edit-panel', 
            array( $this, 'quick_edit_settings_page' ), 
            null, 
            99
        );
    }*/
       

}   
new WooTagsSelectBox();
}