<?php
/*
	Plugin Name: Custom Font Icons
	Description: Adds custom font icons
	Version: 08March16
	License: GPL2
*/


// Register style sheet and scripts.
add_action( 'wp_enqueue_scripts', 'register_customfonticons' );

/**
 * Register style sheet.
 */
function register_customfonticons() {
	//wp_register_style( 'customfonticons', plugins_url( 'css/customfonticons.css' , __FILE__ ) );
	wp_enqueue_style( 'customfonticons' );
}

?>