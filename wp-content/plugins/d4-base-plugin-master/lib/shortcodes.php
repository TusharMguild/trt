<?php
// Use: [button text="" subtext="" link="" icon=""]
	function shortcode_button( $atts ) {
		$attr=shortcode_atts(array(
			'text' => '',
			'subtext'=>'',
			'link' => '',
			'icon' => '',
			'target' => ''
		), $atts);

		if ($attr['text'] != '') {
			$text = '<span class="button-text">'.$attr['text'].'</span>';
		} else {
			$text = '<span class="button-text">Learn More</span>';
		}

		if ($attr['subtext'] != '') {
			$subtext = '<span class="button-subtext">'.$attr['subtext'].'</span>';
		}

		if ($attr['link'] != '') {
			$link = 'href="'.$attr['link'].'"';
		}
		if ($attr['target'] != '') {
			$target = ' target="'.$attr['target'].'"';
		}

		$icon = $attr['icon'];

		$output = '';
		$output .= '<a '.$link.' class="button"'.$target.'><i class="button-icon '.$icon.'"></i>';
		$output .= $text;
		$output .= $subtext;
		$output .= '</a>';

		return $output;
	} add_shortcode( 'button', 'shortcode_button' );

// Use: [getpost postid="" wrapper="" title=""]
	function shortcode_getpost( $atts ) {
		$attr = shortcode_atts( array(
			'postid'=>'',
			'pageid'=>'',
			'wrapperclass'=>'',
			'posttype'=>'',
			'title'=>'',
			'category_name'=>'',
			'number'=>'99',
			'tag'=>'',
			'link'=>'',
			'use_excerpt'=>'',
			'orderby'=>'date',
			'order' => 'DESC',
			'author' => 'false',
			'edit_link' => '',
			'meta_fields' => '',
			'content_order' => 'title,image,body,meta',
			'all_link' => '',
			'button_text' => 'Read More',
			'image_size' => 'thumbnail',
		), $atts );

		$getposts_args = array();
		
		if ($attr['postid'] != '') {
			$postid = $attr['postid'];
			$getposts_args['p'] = $postid;
		}
		if ($attr['pageid'] != '') {
			$pageid = $attr['pageid'];
			$getposts_args['page_id'] = $pageid;
		}
		if ($attr['wrapperclass'] != '') {
			$wrapperclass = $attr['wrapperclass'];
		}
		if ($attr['category_name'] != '') {
			$category_name = $attr['category_name'];
			$getposts_args['category_name'] = $category_name;
		}
		if ($attr['order'] != '') {
			$order = $attr['order'];
			$getposts_args['order'] = $order;
		}
		if ($attr['number'] != '') {
			$number = intval($attr['number']);
			$getposts_args['posts_per_page'] = $number;
		}
		if ($attr['orderby'] != '') {
			$orderby = $attr['orderby'];
			$getposts_args['orderby'] = $orderby;
		}
		if ($attr['tag'] != '1') {
			$tag = $attr['tag'];
			$getposts_args['tag'] = $tag;
		}
		if ($attr['posttype'] != '') {
			$posttype = $attr['posttype'];
			$getposts_args['post_type'] = $posttype;
		}
		if ($attr['author'] == 'true') {
			$user_ID = get_current_user_id();
			$getposts_args['author'] = $user_ID;
		}

		if ($attr['title'] != '') {
			$title = '<h1 class="getpost-title">'.$attr['title'].'</h1>';
		}	 
	
		
		$getposts = new WP_Query($getposts_args);

		while ( $getposts->have_posts() ) { $getposts->the_post();	
		$postid = get_the_id();


		if ($attr['all_link'] == '') {
			$alllink = '<a class="get-all" href="/'.$posttype.'">See all '.$posttype.'</a>';
		} elseif ($attr['all_link'] == 'none') {
			$alllink = '';
		}
		else {
			$alllink = '<a class="get-all" href="'.$attr['all_link'].'">See all '.$posttype.'</a>';
		}

		// Post Thumbnail
		if ( has_post_thumbnail() ) {
			$thumbnail = '<div class="post-thumb">' . get_the_post_thumbnail( $postid, $attr['image_size'] ) . '</div>';
		}

		if ($attr['use_excerpt'] != '') {
			$body = '<div class="getpost-content '.$posttype.'">'.get_the_excerpt().'</div>';
		} else {
			$body = '<div class="getpost-content">'.do_shortcode(get_the_content()).'</div>';
		}

		$posttitle = '<h3 class="getpost-title">'.get_the_title().'</h3>';

		$postdate = '<div class="post-date">posted '.get_the_date('m/d/Y','','',false).'</div>';


		if ($attr['link'] == 'false') {
			$linkopen = '';
			$linkclose = '';
		}
		elseif ($attr['link'] == '') {
			$linkopen = '<a href="'.get_the_permalink().'">';
			$linkclose = "</a>";
		} else {
			$linkopen = $attr['link'];
			$linkclose = "</a>";
		}		

		if ($attr['edit_link'] != '') {
			$editlink =  '<a href="/wp-admin/post.php?post='.$postid.'&action=edit"><i class="fa fa-pencil"></i> Edit</a>';
		}

		if ($attr['meta_fields'] != '') {
			$meta_fields = explode(',', $attr['meta_fields']);
			$post_meta = '<ul class="getpost-meta">';
			foreach ($meta_fields as $key) {				
				$post_meta .= '<li><span class="meta-key">'.$key.': </span><span class="meta-value">'.get_post_meta($postid, $key, true).'</span></li>';
			}
			$post_meta .= '</ul>';
		}

		$readmore = '<a class="getpost-readmore button" href="'.get_the_permalink().'">'.$attr['button_text'].'</a>';

		$content_order = explode(',',$attr['content_order']);

		$content = '';
		foreach ($content_order as $element) {
			
			if ($element == 'title') { $content .= $posttitle; }
			if ($element == 'date') { $content .= $postdate; }
			if ($element == 'image') { $content .= $thumbnail; }
			if ($element == 'body') { $content .= '<div class="getpost-textwrap">'.$body.'</div>'; }
			if ($element == 'meta') { $content .= $post_meta; }
			if ($element == 'readmore') {$content .= $readmore; }
		}	

		//Render output
		
		$output .= '<div class="getpost-wrapper '.$wrapperclass.'">';
		$output .= $linkopen;
		$output .= $content;
		$output .= $linkclose;
		$output .= $editlink;
		$output .= '<div class="clearfix"></div></div>';	

		}
		
		wp_reset_postdata();

		$output .= $alllink;
		return $output;		

	}

add_shortcode( 'getpost', 'shortcode_getpost' );

// Use: [carousel visible='' slides='']
	function shortcode_carousel( $atts, $content = null ) {
		$attr=shortcode_atts(array(
			'visible' => '',
			'slides'  => '.gallery-item'
		), $atts);

		if ($attr['visible'] != '') {
			$visible = $attr['visible'];
		}

		$slides = $attr['slides'];

		// Enqueue script			
			wp_enqueue_script('cycle2-carousel');

		// Cycle Pagers
			$carousel_prev = '<div class="carousel-pagerelement carousel-prev"><i class="icon-chevron-left"></i></div>';
			$carousel_next = '<div class="carousel-pagerelement carousel-next"><i class="icon-chevron-right"></i></div>';

		// Start SLIDE WRAP
			$carousel_open =
				'<div class="carousel-wrap">'.$carousel_prev.'<div '.
					' class="cycle-slideshow"'. // Initializing class, default for cycle2
				#	' data-cycle-pager="#per-slide-template"'. // Connects to #per-slide-template box below
					' data-cycle-slides="'.$slides.'"'. // identifies the class identifier for slides
					' data-pause-on-hover="false"'. // Pause on hover
					' data-timeout="6000"'. // The time between slide transitions in milliseconds.
					' style="position:relative;"'.
					' data-cycle-fx="carousel"'.
					' data-cycle-carousel-visible="'.$visible.'"'.
    				' data-cycle-carousel-fluid="true"'.
    				' data-cycle-next=".carousel-next"'.
    				' data-cycle-prev=".carousel-prev"'.
				'>';

			$carousel_close = '</div>'.$carousel_next.'</div>';
			$carousel_inner = do_shortcode($content);
		

		$output = '';
		$output .= $carousel_open;
		$output .= $carousel_inner;
		$output .= $carousel_close;

		return $output;
	} add_shortcode( 'carousel', 'shortcode_carousel' );	