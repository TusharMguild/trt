<?php

// auto generates a stock code
add_action( 'save_post', 'save_woocommerce_auto_sku', 10, 3 );
function save_woocommerce_auto_sku( $post_id,$post,$update ) {

	if( ! ( wp_is_post_revision( $post_id) || wp_is_post_autosave( $post_id ) ) ) {

		$post_meta = get_post_meta( $post_id, '_sku', true );
		$posttype = get_post_type($post_id);

		//only run for posts that aren't already published
		if($post_meta == '') {
			if ($posttype == 'product') {

				$sku_total = intval(get_option( 'sku_total', '1894' ));
				$new_sku = $sku_total + 1;

				update_option( 'sku_total', $new_sku, $autoload );
				update_post_meta( $post_id, '_sku', $new_sku);

			}
		}
	}
}
/*Add tonis icon*/
function add_toni_class( $classes ){
  global $post;
    $term_list = wp_get_post_terms($post->ID, 'product_cat', array("fields" => "ids"));
    foreach ($term_list as $value) {
      $child_term = get_term( $value, 'product_cat' );
      $parent_term = get_term( $child_term->parent, 'product_cat' );
      //print_r($parent_term);
      $parent = $parent_term->term_id;
      if($value == '748' || $parent == '748')
      $classes[] = 'product_cat-tonis-treasures';
    }
  return $classes;
}
add_filter('post_class','add_toni_class');
?>