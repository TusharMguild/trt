jQuery(document).ready(function($){
	$('.crsl-items').carousel({ visible: 3, itemMinWidth: 400, itemMargin: 10 });

	var defaultLoadingHtml = '<style>.col-centered{float: none;margin: 0 auto;}.loader{border:5px solid #f3f3f3;border-radius:50%;border-top:5px solid #555;width:50px;height:50px;-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite}@-webkit-keyframes "spin"{0%{-webkit-transform:rotate(0deg);}100%{-webkit-transform:rotate(360deg);}}@keyframes "spin"{0%{transform:rotate(0deg);}100%{transform:rotate(360deg);}}</style><div id="load_popup_modal_contant" class="" role="dialog"><div class="modal-dialog modal-md"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div><div id="validation-error"></div><div class="cl"></div><div class="modal-body"><div class="col-lg-1 col-centered"><div class="loader"></div></div></div><div class="modal-footer"></div></div></div></div>';

	window.PostLightBox = function(id){
		id = id || '';
		//var $modal = $('#slide-'+id);
		var $modal = $('#load_popup_modal_show_id');
		//alert(id);
		$modal.modal('show');
		$modal.html(defaultLoadingHtml);
		$.post('/wp-content/plugins/ml-special-area/special-area-popup.php',{'id': id},
		function(data){
			$modal.html(data);
		}, 'html');			
		}

//$('.slideshow').cycle({});
});