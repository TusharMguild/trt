<?php
/**
* Plugin Name: Special Area
* Description: Add post in slider and light box [SpecialAreaSlider].
* Version: 1.0
* Author: MetroGuild
* Author URI: http://www.metroguild.com
*/

 /**
  * Exit if this wasn't accessed via WordPress (aka via direct access)
  */
if (!defined('ABSPATH')) exit;
// Hide Plugin from Network plugins page to prevent network activation of plugin.
/*function ml_hide_network_plugin( $all ) {
    global $current_screen;

    if( $current_screen->is_network ) {
        unset($all['ml-woo-add-column/woo-custom-column.php']);
    }
    return $all;
}
add_filter( 'all_plugins', 'ml_hide_network_plugin' );*/
/**
* Create base class for Plugin.
*/
if ( !(class_exists('SpecialArea')) ) {
class SpecialArea
{
	/**
    * Add appropriate actions.
    */
	public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_theme_menu' ) );
        add_action( 'wp_enqueue_scripts', array( &$this,'PostResponsiveCarousel') );
        add_action( 'init', array( $this,'register_special_shortcodes') );
        add_action('wp_footer', array( $this,'LightBoxDiv') ); 
	}

    /**
    * Add theme setting page
    */
    public function add_theme_menu($attr) {
        $current_user = wp_get_current_user(); 
        if($current_user->roles[0] == 'shop_manager') {
            $manage_options = 'shop_manager';
        }else{
            $manage_options ='manage_options';
        }             
        add_menu_page(
            'Global Area', 
            'Global Area', 
            $manage_options, 
            'special-area', 
            array( $this, 'special_area_settings_page' ), 
            null, 
            99
        );
    }
    /**
    * Theme setting page callback
    */
    public function special_area_settings_page() {
        if (isset($_POST['Submit'])) {
                $deprecated = null;
                $autoload = 'no';
                // For SpecialText
                if(($_POST['SpecialTitle'] != '')){
                    if ( (get_option( 'SpecialTitle' ) !== false)){
                        update_option( 'SpecialTitle', $_POST['SpecialTitle'] );
                    }else{
                        add_option( 'SpecialTitle', $_POST['SpecialTitle'], $deprecated, $autoload );
                    }
                }
                // End SpecialText
                // For StoreHours
                if(($_POST['StoreHours'] != '')){
                    if ( (get_option( 'StoreHours' ) !== false)){
                        update_option( 'StoreHours', $_POST['StoreHours'] );
                    }else{
                        add_option( 'StoreHours', $_POST['StoreHours'], $deprecated, $autoload );
                    }
                }
                // End StoreHours

        }
        ?>
        <div class="wrap">
            <h2>Area for Global Content</h2>
        <form method="post" action="">
            <p><strong>Special Area Title</strong><br />
                <input type="text" name="SpecialTitle" size="45" value="<?php echo $SpecialTitle = (get_option( 'SpecialTitle' ) !== false) ? get_option( 'SpecialTitle' ) : '' ;?>" />
            </p> 
            <p><strong>Store Hours</strong><br />
                <textarea rows="4" cols="47" name="StoreHours"><?php echo $StoreHours = (get_option( 'StoreHours' ) !== false) ? get_option( 'StoreHours' ) : '' ;?></textarea>
            </p>          
            <p><input type="submit" name="Submit" value="Submit" /></p>
        </form>
        </div>
        <?php
    }    
    /**
    * Add script into theme header
    */
    public function PostResponsiveCarousel() {
        //wp_enqueue_script( 'core', plugin_dir_url( __FILE__ ) . 'js/jquery-1.9.1.min.js', array( 'jquery' ), '1.9.1', true );
        wp_enqueue_script( 'Bootstrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), '1.0.0', true );
        //wp_enqueue_script( 'responsiveCarousel', plugin_dir_url( __FILE__ ) . 'js/responsiveCarousel.min.js', array( 'jquery' ), '1.0.0', true );
        wp_enqueue_script( 'Custom-js', plugin_dir_url( __FILE__ ) . 'js/postCarousel.js', array( 'jquery' ), '1.0.0', true );
        wp_enqueue_style( 'Bootstrap-css', plugins_url('css/bootstrap.min.css', __FILE__) );        
    }

    /**
    * Create shortcode for special area
    */
    public function register_special_shortcodes(){
        add_shortcode( 'SpecialAreaSlider', array($this,'specialarea_shortcode'));
        add_shortcode( 'StoreHours', array($this,'storehours_shortcode'));
    }
    public function specialarea_shortcode(){
        $slidequery = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 5,
                'order' => 'DESC',
            ));
        if ( $slidequery->have_posts() ) :
        $out = '<div id="featured-products" class="fullwidth limitedSpecials">
    <div class="skivdiv-content">
        <h1>'.get_option( 'SpecialTitle' ).'</h1>
        <div class="carousel-wrap">
        <div class="carousel-pagerelement special-prev"><i class="icon-chevron-left"></i></div>
        <div class="slideshow cycle-slideshow" data-cycle-slides=".post" data-pause-on-hover="false" data-timeout="0" style="position: relative; overflow: hidden;" data-cycle-fx="carousel" data-cycle-carousel-visible="3" data-cycle-carousel-fluid="true" data-cycle-next=".special-next" data-cycle-prev=".special-prev">            
        <div class="cycle-carousel-wrap">'; 
        /*$out .='<div class="cycle-carousel-wrap">';*/
       while ( $slidequery->have_posts() ) : $slidequery->the_post();
        $featured_img_url = get_the_post_thumbnail_url($slidequery->post->ID,'full');
        $props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
        $image            = get_the_post_thumbnail( $slidequery->post->ID, 'full', array(
                'title'  => $props['title'],
                'alt'    => $props['alt'],                
            ) );
        if(empty($image)){
            $NoImage = 'NoImage';
            $image = '<img src="'.plugins_url( 'image/default-image.jpg', __FILE__ ).'" alt="Default-Image" class="attachment-full size-full wp-post-image">';
        }else{
            $image;
            $NoImage = '';
        }
        $value = get_post_meta( $slidequery->post->ID, 'post_summary', true );
        //$value = get_field( "post_summary", $slidequery->post->ID );
        $excerpt = get_the_content();
        $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
        $permalink = get_permalink($post->ID);
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        if(!empty($image)){
            $excerpt = substr($excerpt, 0, 150);
        }else{
            $excerpt = substr($excerpt, 0, 400);
        }
        //$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));  
        $excerpt = $excerpt.' <span class="rmore">&raquo; read more<span>';     
        $out .= '<li class="' . join( ' ', get_post_class( "cycle-slide $NoImage", $slidequery->post->ID ) ) . '" onclick="PostLightBox('.$slidequery->post->ID.')"><a href="javascript:void(0)" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                '.$image.'
            <h2 class="woocommerce-loop-product__title">'.get_the_title().'</h2>
            <div class="PostText">'.$excerpt.'</div>
            </a>            
            </li>';       
        
        endwhile; // end of the loop. 
       /*$out .= '</div>';*/
       $out .= '</div>
        </div>
        <div class="carousel-pagerelement special-next"><i class="icon-chevron-right"></i></div>
        </div>
<div class="clear"></div>
</div>
</div>';
    endif;
        wp_reset_postdata();
    return $out;
    }
    
    /**
    * Add div for light to footer.
    */
    public function LightBoxDiv() {
        $lightBoxDiv = '<div id="load_popup_modal_show_id" class="modal fade" tabindex="-1"></div>';
        echo $lightBoxDiv;
    }
    /**
    * Store hours shortcode function.
    */
    public function storehours_shortcode(){
        $out = '<p>'.get_option( 'StoreHours' ).'</p>';
        return $out;
    }     
    //End    
}
new SpecialArea();
}