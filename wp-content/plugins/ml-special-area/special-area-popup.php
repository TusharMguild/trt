<?php require_once("../../../wp-load.php"); ?>
  <div id="load_popup_modal_contant" class="" role="dialog">

  <div class="modal-dialog modal-md">
    <?php
    $id = $_POST["id"];
    //$id2 = $_POST["id2"];
    ?>
    <!-- Start: Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo get_the_title($id); ?></h4>
      </div>
        <div id="validation-error"></div>
  <div class="cl"></div>
        <div class="modal-body">
          <?php
          $image            = get_the_post_thumbnail( $id, 'full', array(
                'title'  => $props['title'],
                'alt'    => $props['alt'],                
            ) );
          if (!empty($image)) {
            echo $image;
          }else{}
            $content_post = get_post($id);
            $content = $content_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content; 
          ?>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
  </div>