<?php
/**
* Plugin Name: Woocommerce Custom Column
* Description: Add custom column for woocommerce product listing page.
* Version: 1.0
* Author: MetroGuild
* Author URI: http://www.metroguild.com
*/

 /**
  * Exit if this wasn't accessed via WordPress (aka via direct access)
  */
if (!defined('ABSPATH')) exit;
// Hide Plugin from Network plugins page to prevent network activation of plugin.
/*function ml_hide_network_plugin( $all ) {
    global $current_screen;

    if( $current_screen->is_network ) {
        unset($all['ml-woo-add-column/woo-custom-column.php']);
    }
    return $all;
}
add_filter( 'all_plugins', 'ml_hide_network_plugin' );*/
/**
* Create base class for Plugin.
*/
if ( !(class_exists('WooCustomColumn')) ) {
class WooCustomColumn
{
	/**
    * Add appropriate actions.
    */
	public function __construct() {
		/*add_action( 'admin_menu', array( $this, 'add_setting_page' ) );
        add_action( 'admin_footer', array( $this, 'setting_init' ) );
        add_action( 'wp_footer', array( $this, 'setting_init' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'load_jquery_library' ) );*/
        add_filter( 'manage_edit-product_columns', array( $this,'add_columns_filter'),10, 1 );
        add_action( 'manage_product_posts_custom_column', array( $this,'custom_column'), 10, 2 );
        add_filter( 'manage_edit-product_sortable_columns', array( $this,'sortable_columns') );
        add_action( 'pre_get_posts', array( $this,'sku_column_orderby') );
        add_action('acf/load_field/name=product_sku', array( $this,'set_sku_value'), 10, 3);
	}
    /**
    * Add custom Column.
    */
    public function add_columns_filter($columns){
        $columns['product_sku'] = __( 'Item Number');
        unset($columns['sku']); 
        return $columns;
    }
    /**
    * Add custom Column value.
    */
    public function custom_column($column, $postid){
        if ( $column == 'product_sku' ) {
            echo get_field( 'product_sku', $postid );
            //echo get_post_meta( $postid, 'product_sku', true );
        }
    }
    /**
    * Custom column sortable.
    */
    public function sortable_columns($columns){
        $columns[ 'product_sku' ] = 'product_sku';
        return $columns;
    }
    /**
    * Custom column sortable Query.
    */
    public function sku_column_orderby($query){
        if( ! is_admin() )
        return;
        $orderby = $query->get( 'orderby');
        if( 'product_sku' == $orderby ) {
            $query->set('meta_key','product_sku');
            $query->set('orderby','meta_value_num');
        }
    }
    /**
    * Set the value if sku not have value.
    */
    public function set_sku_value($field){
        global $post, $wpdb;
        $results = $wpdb->get_results( 'SELECT * FROM wp_postmeta WHERE meta_key = "product_sku" ORDER BY meta_value DESC LIMIT 1');
        $default_val = $results[0]->meta_value+1;
        if( get_post_meta($post->ID, 'product_sku', true) === null || get_post_meta($post->ID, 'product_sku', true) == '' ) {
            $field['value'] = $default_val;
        }
        return $field;
    }
}
new WooCustomColumn();
}