<?php
/**
* Plugin Name: Quick Edit Option
* Description: This plugin help you to add ACF field into post or custom post type quick edit.
* Version: 1.0
* Author: MetroGuild
* Author URI: http://www.metroguild.com
*/

 /**
  * Exit if this wasn't accessed via WordPress (aka via direct access)
  */
if (!defined('ABSPATH')) exit;
// Hide Plugin from Network plugins page to prevent network activation of plugin.
/*function ml_hide_network_plugin( $all ) {
    global $current_screen;

    if( $current_screen->is_network ) {
        unset($all['ml-woo-add-column/woo-custom-column.php']);
    }
    return $all;
}
add_filter( 'all_plugins', 'ml_hide_network_plugin' );*/
/**
* Create base class for Plugin.
*/
if ( !(class_exists('QuickEditOption')) ) {
class QuickEditOption
{
	/**
    * Add appropriate actions.
    */
	public function __construct() {
        global $QuickEditFields, $post, $post_id;
        $post_id = $post->ID;
        $QuickEditFields = get_option( 'QuickEditFields' );
        $QuickEditFields = explode(",", $QuickEditFields);
        add_action( 'admin_menu', array( $this, 'add_quick_edit_menu' ) );
        add_action( 'quick_edit_custom_box', array( $this, 'quantity_quickedit_fields' ), 10, 2 );
        add_action( 'save_post', array( $this, 'quantity_quickedit_save_post' ), 10, 2 );
        add_action( 'admin_print_footer_scripts-edit.php', array( $this, 'quantity_quickedit_javascript' ) );
        add_filter('post_row_actions', array( $this, 'quantity_quickedit_set_data' ), 10, 2);
        /*add_action( 'admin_menu', array( $this, 'add_theme_menu' ) );
        add_action( 'wp_enqueue_scripts', array( &$this,'PostResponsiveCarousel') );
        add_action( 'init', array( $this,'register_special_shortcodes') );
        add_action('wp_footer', array( $this,'LightBoxDiv') );*/
	}
    /**
    * Add theme setting page
    */
    public function add_quick_edit_menu($attr) {             
        add_menu_page(
            'Quick Edit Panel', 
            'Quick Edit Panel', 
            'manage_options', 
            'quick-edit-panel', 
            array( $this, 'quick_edit_settings_page' ), 
            null, 
            99
        );
    }
    /**
    * Create html for field.
    */
    public function quantity_quickedit_fields( $column_name, $post_type) {
      global $QuickEditFields, $wpdb;
      //$column = explode(",", $this->$column);      
        if ( 'name' != $column_name )
            return;       
        ?>
        <fieldset class="inline-edit-col-left">
            <div class="inline-edit-col">
                <label>
                    <span class="title"><?php esc_html_e( 'Item Number', 'product_sku' ); ?></span>
                    <span class="input-text-wrap">
                        <input type="text" name="product_sku_edit" class="product_sku" value="">
                    </span>
                </label>
            </div>
        </fieldset>
        <fieldset class="inline-edit-col-left">
            <div class="inline-edit-col">
                <label>
                    <span class="title"><?php esc_html_e( 'Quantity', 'quantity' ); ?></span>
                    <span class="input-text-wrap">
                        <input type="text" name="quantity_edit" class="quantity" value="">
                    </span>
                </label>
            </div>
        </fieldset>        

        <fieldset class="inline-edit-col-left">
            <div class="inline-edit-col">
                <label>
                    <span class="title"><?php esc_html_e( 'Condition', 'product_condition' ); ?></span>
                    <span class="input-text-wrap">
                        <input type="hidden" name="product_condition_noncename" id="product_condition_noncename" value="" />
                        <select name="product_condition_edit" class="product_condition" id="product_condition">
                            <option class='condition-option' value="">Please select</option>
                            <option class='condition-option' value="Looks New">Looks New</option>
                            <option class='condition-option' value="Very Clean">Very Clean</option>
                            <option class='condition-option' value="Perfect Condition">Perfect Condition</option>
                            <option class='condition-option' value="Great Condition">Great Condition</option>
                            <option class='condition-option' value="Good Condition">Good Condition</option>
                            <option class='condition-option' value="Fair Condition">Fair Condition</option>
                            <option class='condition-option' value="New">New</option>
                            <option class='condition-option' value="Unique">Unique</option>
                            <option class='condition-option' value="Refurbished">Refurbished</option>
                            <option class='condition-option' value="Condition Varies">Condition Varies</option>
                        </select>
                    </span>
                </label>
            </div>
        </fieldset>

        <fieldset class="inline-edit-col-left">
            <div class="inline-edit-col">
                <label>
                    <span class="title"><?php esc_html_e( 'Price Range', 'price_range' ); ?></span>
                    <span class="input-text-wrap">
                        <input type="text" name="price_range_edit" class="price_range" value="">
                    </span>
                </label>
            </div>
        </fieldset>
        <fieldset class="inline-edit-col-left">
            <div class="inline-edit-col">
                <label>
                    <span class="title"><?php esc_html_e( 'Appointment', 'by_appointment_sel' ); ?></span>
                    <span class="input-text-wrap">
                        <input type="hidden" name="by_appointment_noncename" id="by_appointment_noncename" value="" />
                        <select name="by_appointment_sel_edit" class="by_appointment_sel" id="by_appointment_sel">
                            <option class='appointment-option' value="No">No</option>
                            <option class='appointment-option' value="Yes">Yes</option>
                        </select>
                    </span>
                </label>
            </div>
        </fieldset>
        <!-- <fieldset class="inline-edit-col-left" id="AppointmentFieldId">
            <div class="inline-edit-col">
                <label>
                    <span class="title"><?php esc_html_e( 'Appointment', 'by_appointment' ); ?></span>
                    <span class="input-text-wrap">
                        <input type="checkbox" name="by_appointment_edit" class="by_appointment" value="1" >
                    </span>
                </label>
            </div>            
        </fieldset> -->

        <!-- <fieldset class="inline-edit-col-left">
            <legend class="inline-edit-legend"><?php esc_html_e( 'Product display options', 'product_dispaly' ); ?></legend>
            <div class="inline-edit-col">
                <label>
                    <input type="checkbox" name="hot_buy_edit" class="hot_buy" value="">
                    <span class="checkbox-title">Hot buy</span>
                </label>
            </div>
            <div class="inline-edit-col">
                <label>
                    <input type="checkbox" name="featured_product_edit" class="featured_product" value="">
                    <span class="checkbox-title">Featured</span>
                </label>
            </div>

        </fieldset> -->

        <?php
    }
    /**
    * Save field
    */
    public function quantity_quickedit_save_post( $post_id, $post ) {
        // if called by autosave, then bail here
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return;

        // if this "post" post type?
        if ( $post->post_type != 'product' )
            return;
          
        // does this user have permissions?
         if ( ! current_user_can( 'edit_post', $post_id ) )
             return;

        // update!
        if ( isset( $_POST['quantity_edit'] ) ) {
          update_field( 'quantity', $_POST['quantity_edit'], $post_id );
        }
        if ( isset( $_POST['price_range_edit'] ) ) {
          update_field( 'price_range', $_POST['price_range_edit'], $post_id );
        }
        if ( isset( $_POST['product_sku_edit'] ) ) {
          update_field( 'product_sku', $_POST['product_sku_edit'], $post_id );
        }

        if ( isset( $_POST['product_condition_edit'] ) ) {
          update_field( 'product_condition', $_POST['product_condition_edit'], $post_id );
        }
        if ( isset( $_POST['by_appointment_sel_edit'] ) ) {
          update_field( 'by_appointment_sel', $_POST['by_appointment_sel_edit'], $post_id );
        }
        /*$appointmentdata = empty( $_POST['by_appointment_edit'] ) ? 0 : 1;
        if ($appointmentdata==1){
            update_post_meta($post_id, 'by_appointment', '1');
        }else{
            update_post_meta($post_id, 'by_appointment', '0');
        }*/ 
        /*$hotbuydata = empty( $_POST['hot_buy_edit'] ) ? 0 : 1;
        if ( $hotbuydata ) {
          update_field( 'hot_buy', $_POST['hot_buy_edit'], $post_id );
        }
        $val = 'a:1:{i:0;s:3:"206";}';
        $fproductdata = empty( $_POST['featured_product_edit'] ) ? $val : $val;
        if ( $fproductdata ) {
          update_field( 'featured_product', $_POST['featured_product_edit'], $post_id );
        }*/
    }
    /**
    * Add script for field
    */
    public function quantity_quickedit_javascript() {
            $current_screen = get_current_screen();
           //print_r($current_screen);
            if ( $current_screen->id != 'edit-product' || $current_screen->post_type != 'product' )
                return;

            // Ensure jQuery library loads
            wp_enqueue_script( 'jquery' );
            ?>
            <script type="text/javascript">
                jQuery( function( $ ) {
                    $( '#the-list' ).on( 'click', 'a.editinline', function( e ) {               
                        e.preventDefault();
                        var quantity_val = $(this).data( 'quantity' );
                        var price_range_val = $(this).data( 'price_range' );
                        var product_sku_val = $(this).data( 'product_sku' );
                        var product_condition_val = $(this).data( 'product_condition' );
                        var by_appointment_sel_val = $(this).data( 'by_appointment_sel' );
                        //var by_appointment_val = $(this).data( 'by_appointment' );
                        /*var productConditionInput = document.getElementById('product_condition');
                        var nonceInput = document.getElementById('product_condition_noncename');
                        //alert($('#product_condition option').length);
                        nonceInput.value = nonce;*/
                        /*var hot_buy_val = $(this).data( 'hot_buy' );
                        var featured_product_val = $(this).data( 'featured_product' );*/
                        inlineEditPost.revert();
                        $("#product_condition option").each(function()
                        {
                            if($(this).val() == product_condition_val){
                                $(this).attr('selected', true);
                            } else {
                                $(this).attr('selected', false);
                            }
                        });
                        $("#by_appointment_sel option").each(function()
                        {
                            if($(this).val() == by_appointment_sel_val){
                                $(this).attr('selected', true);
                            } else {
                                $(this).attr('selected', false);
                            }
                        });
                        $( '.quantity' ).val( quantity_val ? quantity_val : '' );
                        $( '.price_range' ).val( price_range_val ? price_range_val : '' );
                        $( '.product_sku' ).val( product_sku_val ? product_sku_val : '' );
                        $( '.product_condition' ).val( product_condition_val ? product_condition_val : '' );
                        $( '.by_appointment_sel' ).val( by_appointment_sel_val ? by_appointment_sel_val : '' );
                        //$( '.by_appointment' ).attr( 'checked', (0 == by_appointment_val) || (typeof by_appointment_val === "undefined") ? false : true);
                        /*$( '.hot_buy' ).attr( 'checked', 0 == hot_buy_val ? false : true );
                        $( '.featured_product' ).attr( 'checked', 0 == featured_product_val ? false : true );*/
                    });
                });
            </script>
    <?php
    }
    /**
    * Set value for field
    */
    public function quantity_quickedit_set_data( $actions, $post ) {
        $found_value = get_post_meta( $post->ID, 'quantity', true );
        $price_range = get_post_meta( $post->ID, 'price_range', true );
        $product_sku = get_post_meta( $post->ID, 'product_sku', true );
        $product_condition = get_post_meta( $post->ID, 'product_condition', true );
        $by_appointment_sel_val = get_post_meta( $post->ID, 'by_appointment_sel', true );
        //$by_appointment = get_post_meta( $post->ID, 'by_appointment', true );
        /*$hot_buy = get_post_meta( $post->ID, 'hot_buy', true );
        $featured_product = get_post_meta( $post->ID, 'featured_product', true );*/
        if ( $found_value ) {
            if ( isset( $actions['inline hide-if-no-js'] ) ) {
                $new_attribute = sprintf( 'data-quantity="%s"', esc_attr( $found_value ) );
                $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
            }
        }

        if ( $price_range ) {
            if ( isset( $actions['inline hide-if-no-js'] ) ) {
                $new_attribute = sprintf( 'data-price_range="%s"', esc_attr( $price_range ) );
                $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
            }
        }
        if ( $product_sku ) {
            if ( isset( $actions['inline hide-if-no-js'] ) ) {
                $new_attribute = sprintf( 'data-product_sku="%s"', esc_attr( $product_sku ) );
                $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
            }
        }
        if ( $product_condition ) {
            if ( isset( $actions['inline hide-if-no-js'] ) ) {
                $new_attribute = sprintf( 'data-product_condition="%s"', esc_attr( $product_condition ) );
                $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
            }
        }
        if ( $by_appointment_sel_val ) {
            if ( isset( $actions['inline hide-if-no-js'] ) ) {
                $new_attribute = sprintf( 'data-by_appointment_sel="%s"', esc_attr( $by_appointment_sel_val ) );
                $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
            }
        }
       /* if ( $by_appointment ) {
            if ( isset( $actions['inline hide-if-no-js'] ) ) {
                $new_attribute = sprintf( 'data-by_appointment="%s"', esc_attr( $by_appointment ) );
                $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
            }
        }*/       

        return $actions;
    }
    public function quick_edit_settings_page() {
    ?>
        <div class="wrap">
            Work is going on......
            <h2>Quick Edit Option</h2>
            <?php
            if (isset($_POST['Submit'])) {
                $deprecated = null;
                $autoload = 'no';
                // For SpecialText
                if(($_POST['QuickEditFields'] != '')){
                    if ( (get_option( 'QuickEditFields' ) !== false)){
                        update_option( 'QuickEditFields', $_POST['QuickEditFields'] );
                    }else{
                        add_option( 'QuickEditFields', $_POST['QuickEditFields'], $deprecated, $autoload );
                    }
                }
            } 
            /*global $wpdb;
              $result = $wpdb->get_results($wpdb->prepare(
                  "SELECT post_id,meta_id,meta_key,meta_value FROM wp_posts,wp_postmeta WHERE post_type = %s
                    AND wp_posts.ID = wp_postmeta.post_id", 'product'
              ), ARRAY_A);
              echo "<pre>";
              print_r($result);
              echo "</pre>";
              $v = $wpdb->get_results($wpdb->prepare(
                  "SELECT meta_value FROM wp_postmeta WHERE meta_key = %s LIMIT 1", '_product_condition1'
              ), ARRAY_A);
              print_r($v);
              $field = get_field_object($v[0]['meta_value'], '66411');
              echo "<pre>";
              print_r($field); 
              echo "</pre>";*/
              ?>
            <form method="post" action="">
                <p><strong>Special Text</strong><br />
                <input type="text" name="QuickEditFields" size="45" value="<?php echo $QuickEditFields = (get_option( 'QuickEditFields' ) !== false) ? get_option( 'QuickEditFields' ) : '' ;?>" /><br />

            </p>
                <p><input type="submit" name="Submit" value="Submit" /></p>                
            </form>           
        </div>
    <?php    
    }    

}   
new QuickEditOption();
}