<?php
/*
	Plugin Name: Socialbox
	Plugin URI: https://github.com/atelierabbey/Socialbox
	GitHub Theme URI: https://github.com/atelierabbey/Socialbox
	GitHub Branch: master
	Description: Simple Social Media & Contact info admin info page for WordPress
	Version: 01Apr15
	Author: Grayson A.C. Laramore
	Author URI: http://wwww.SillyCoyote.com
	License: GPL2
*/



$list_o_social = array( 'RSS', 'Etsy', 'Facebook', 'Flickr', 'Google Plus', 'Instagram', 'LinkedIn', 'Pinterest', 'Twitter', 'Vimeo', 'Youtube' );
$icon_location = plugins_url( '/img/social/' , __FILE__ );
$css_location = plugins_url( '/css/socialbox.css' , __FILE__ );

include ('lib/socialbox.php');

new skivvy_socialbox;

?>
